/* Edited from fcft source code. Released under MIT license. */

#include <stdio.h>
#include <string.h>
#include <fontconfig/fontconfig.h>
#include <stdlib.h>

#define LOG_ERR(...) fprintf(stderr, __VA_ARGS__)

static char* file_from_pattern(FcPattern *pattern, int *index) {
	FcChar8 *face_file = NULL;
	if (FcPatternGetString(pattern, FC_FT_FACE, 0, &face_file) != FcResultMatch &&
		FcPatternGetString(pattern, FC_FILE, 0, &face_file) != FcResultMatch)
		LOG_ERR("no face file path in pattern");
	*index = 0;
	FcPatternGetInteger(pattern, FC_INDEX, 0, index);
	return strdup((void*)face_file);
}

static FcPattern* base_pattern_from_name(const char *name, FcFontSet **set) {
	FcPattern *pattern = FcNameParse((const unsigned char *)name);
	if (pattern == NULL) {
		fprintf(stderr, "%s: failed to lookup font", name);
		return NULL;
	}
	if (!FcConfigSubstitute(NULL, pattern, FcMatchPattern)) {
		fprintf(stderr, "%s: failed to do config substitution", name);
		FcPatternDestroy(pattern);
		return NULL;
	}
	FcDefaultSubstitute(pattern);
	FcResult result;
	*set = FcFontSort(NULL, pattern, FcTrue, NULL, &result);
	if (result != FcResultMatch) {
		fprintf(stderr, "%s: failed to match font", name);
		FcPatternDestroy(pattern);
		return NULL;
	}
	return pattern;
}

static FcPattern* pattern_from_font_set(FcPattern *base_pattern, FcFontSet *set, int idx) {
	FcPattern *pattern = FcFontRenderPrepare(NULL, base_pattern, set->fonts[idx]);
	if (pattern == NULL) {
		LOG_ERR("failed to prepare 'final' pattern");
		return NULL;
	}
	return pattern;
}

struct __fontinfo {
	int nfont;
	char **fontfiles;
	int *faceindices;
	FcCharSet **charsets;
};

static struct __fontinfo get_fonts(const char *fonttype) {
	FcFontSet *set;
	FcPattern *base_pattern = base_pattern_from_name(fonttype, &set);
	int nfont = set->nfont;
	FcCharSet **charsets = malloc(nfont * sizeof(void*));
	char **fontfiles = malloc(nfont * sizeof(char*));
	int *faceindices = malloc(nfont * sizeof(int));

	for (int ifont=0; ifont<nfont; ifont++) {
		FcPattern *pattern = pattern_from_font_set(base_pattern, set, ifont);
		fontfiles[ifont] = file_from_pattern(pattern, faceindices+ifont);
		if (FcPatternGetCharSet(base_pattern, FC_CHARSET, 0, charsets+ifont) != FcResultMatch &&
			FcPatternGetCharSet(pattern, FC_CHARSET, 0, charsets+ifont) != FcResultMatch)
			LOG_ERR("%s: failed to get charset", fontfiles[ifont]);
		FcPatternDestroy(pattern);
	}
	FcPatternDestroy(base_pattern);
	FcFontSetDestroy(set);
	return (struct __fontinfo) {nfont, fontfiles, faceindices, charsets};
}

#ifdef fonttitesti
int main() {
	struct __fontinfo fontinfo = get_fonts("mono:bold:italic");
	for (int ifont=0; ifont<fontinfo.nfont; ifont++)
		printf("🔇(%i) ⁻(%i): %s\n",
			FcCharSetHasChar(fontinfo.charsets[ifont], U'🔇'),
			FcCharSetHasChar(fontinfo.charsets[ifont], U'⁻'), fontinfo.fontfiles[ifont]);

	for (int i=0; i<fontinfo.nfont; i++) {
		FcCharSetDestroy(fontinfo.charsets[i]);
		free(fontinfo.fontfiles[i]);
	}
	free(fontinfo.charsets);
	free(fontinfo.fontfiles);
	free(fontinfo.faceindices);
}
#endif
