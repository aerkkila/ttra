#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#define __included_freetype2__
#include <fontconfig/fontconfig.h>
#define __included_fontconfig__
#include "ttra.h"

#define min(a,b) ((a) < (b) ? a : (b))

static void newline(struct ttra *ttra);

#include "ttra_rendering.c"
#include "colors.c"

#define tabwidth 8

static void erase_char(struct ttra *ttra) {
	if (!ttra->canvas)
		return;
	int y0 = ttra->y0 + ttra->j * ttra->fontheight;
	int x0 = ttra->x0 + ttra->i * ttra->fontwidth;
	int smallest_y = min(y0, ttra->y0 + ttra->h);
	int biggest_y = min(y0 + ttra->fontheight, ttra->y0 + ttra->h);
	int smallest_x = min(x0, ttra->x0 + ttra->w);
	int biggest_x = min(x0+ttra->fontwidth, ttra->x0 + ttra->w);
	for (int y=smallest_y; y<biggest_y; y++)
		for (int x=smallest_x; x<biggest_x; x++)
			ttra->canvas[y*ttra->realw + x] = ttra->bg_color;
}

static void erase_line(struct ttra *ttra, int how) {
	if (!ttra->canvas)
		return;
	int i0 = ttra->i;
	switch (how) {
		case 2:
			ttra->i = 0; // no break
		default:
		case 0:
			while (!ttra_eol(ttra)) erase_char(ttra), ttra->i++;
			break;
		case 1:
			while (ttra->i >= 0) erase_char(ttra), ttra->i--;
			break;
	}
	ttra->i = i0;
}

static void save_state(struct ttra *ttra) {
	ttra->isave = ttra->i;
	ttra->jsave = ttra->j;
	ttra->xsave = ttra->x;
}

static void restore_state(struct ttra *ttra) {
	ttra->i = ttra->isave;
	ttra->j = ttra->jsave;
	ttra->x = ttra->xsave;
}

static unsigned make_color(int *params) {
	return params[0] << 24 | params[1] << 16 | params[2] << 8 | 0xff << 0;
}

static void reset_style(struct ttra *ttra) {
	ttra->style = 0;
	ttra->fontstyle = regularfont_e;
	ttra->fg_color = ttra->fg_default;
	ttra->bg_color = ttra->bg_default;
	end_scriptsize(ttra);
}

static void set_style(int *params, int nparams, struct ttra *ttra) {
	while (nparams > 0) {
		int nparams_thistime = 1;
		switch (params[0]) {
			case normal_e:
				reset_style(ttra);
				break;
			case bold_e:
				ttra->style |= 1<<bold_e;
				ttra->fontstyle |= boldfont_e;
				break;
			case faint_e:
				ttra->style |= 1<<faint_e;
				break;
			case italic_e:
				ttra->style |= 1<<italic_e;
				ttra->fontstyle |= italicfont_e;
				break;
			case underlined_e:
				ttra->style |= 1<<underlined_e;
				break;
			case invert_e:
				ttra->style |= 1<<invert_e;
				break;
			case superscript_e:
				ttra->style |= (__int128)1<<superscript_e;
				break;
			case subscript_e:
				ttra->style |= (__int128)1<<subscript_e;
				break;

			case not_italic_e:
				ttra->style &= ~(1<<italic_e);
				ttra->fontstyle &= ~italicfont_e;
				break;
			case not_bold_e:
				ttra->style &= ~(1<<bold_e);
				ttra->fontstyle &= ~boldfont_e;
				break;
			case not_underlined_e:
				ttra->style &= ~(1<<underlined_e);
				break;
			case not_invert_e:
				ttra->style &= ~(1<<invert_e);
				break;
			case not_script_e:
				ttra->style &= ~((__int128)1<<superscript_e | (__int128)1<<subscript_e);
				end_scriptsize(ttra);
				break;

			case 38:
				if (nparams <= 1)
					return;
				if (params[1] == 5) {
					if (nparams > 2)
						ttra->fg_color = color256[params[2]];
					nparams_thistime = 3;
				}
				else if (params[1] == 2) {
					if (nparams > 4)
						ttra->fg_color = make_color(params+2);
					nparams_thistime = 5;
				}
				break;

			case 48:
				if (nparams <= 1)
					return;
				if (params[1] == 5) {
					if (nparams > 2)
						ttra->bg_color = color256[params[2]];
					nparams_thistime = 3;
				}
				else if (params[1] == 2) {
					if (nparams > 4)
						ttra->bg_color = make_color(params+2);
					nparams_thistime = 5;
				}
				break;

			default:
				if (30 <= params[0] && params[0] <= 37)
					ttra->fg_color = color8dim[params[0] - 30];
				else if (40 <= params[0] && params[0] <= 47)
					ttra->bg_color = color8dim[params[0] - 40];
				else if (90 <= params[0] && params[0] <= 97)
					ttra->fg_color = color8bright[params[0] - 90];
				else if (100 <= params[0] && params[0] <= 107)
					ttra->bg_color = color8bright[params[0] - 100];
		}
		nparams -= nparams_thistime;
		params += nparams_thistime;
	}
}

#define maxparams 16

static const void* escape(struct ttra *ttra, const char *str) {
	int params[maxparams] = {0}, nparams = 1, bool_isdefault = 1;
	if (*str == '[')
		str++; // traditional escape code begins with \e[ but I allow omitting the '['

loop:
	switch (*str) {
		case 'A': ttra->j -= params[0] + bool_isdefault; break;
		case 'B': ttra->j += params[0] + bool_isdefault; break;
		case 'C': ttra->i += params[0] + bool_isdefault; break;
		case 'D': ttra->i -= params[0] + bool_isdefault; break;
		case 'm': set_style(params, nparams, ttra); break;
		case 'K': erase_line(ttra, params[0]); break;
		case 's': save_state(ttra); break;
		case 'u': restore_state(ttra); break;
		case '$': ttra->mathmode=1; break;
		case ';':
		case ':':
				  if (++nparams > maxparams)
					  return str;
				  str++;
				  goto loop;
		default:
				  if ('0' <= *str && *str <= '9') {
					  params[nparams-1] = params[nparams-1]*10 + *str - '0';
					  str++;
					  bool_isdefault = 0;
					  goto loop;
				  }
				  else
					  return str; // unknown escape code
	}

	if (ttra->max_i < ttra->i)
		ttra->max_i = ttra->i;
	if (ttra->max_j < ttra->j)
		ttra->max_j = ttra->j;

	return str + 1;
}

static void newline(struct ttra *ttra) {
	ttra->i = 0;
	ttra->j++;
	if (ttra->max_j < ttra->j)
		ttra->max_j = ttra->j;
}

static void carriage_return(struct ttra *ttra) {
	ttra->i = 0;
}

static void tabular(struct ttra *ttra) {
	ttra->i = ((ttra->i+1) / tabwidth + !!((ttra->i+1) % tabwidth)) * tabwidth;
	if (ttra->max_i < ttra->i)
		ttra->max_i = ttra->i;
}

int ttra_eol(struct ttra *ttra) {
	return (ttra->i+1) * ttra->fontwidth >= ttra->w;
}

int ttra_lastline(struct ttra *ttra) {
	return (ttra->j+1) * ttra->fontheight >= ttra->h;
}

int ttra_end(struct ttra *ttra) {
	return ttra_lastline(ttra) && ttra_eol(ttra);
}

int ttra_set_this_fontheight(struct ttra *ttra, int ind) {
	FT_Face face = ttra->ft_faces[ind];
	int destheight = ttra->fontheight;
	int try = destheight, last_try, heightnow;
	do {
		if (FT_Set_Pixel_Sizes(face, 0, try))
			return destheight; // Error which happens with color emojis. I skip them.
		heightnow = face->size->metrics.height / 64;
		last_try = try;
		try = destheight * try / heightnow;
	} while (try != destheight && last_try != try);
	return ttra->current_fontheight[ind] = heightnow;
}

int ttra_set_fontheight(struct ttra *ttra, int height) {
	ttra->fontheight = height;

	int fontind = 0;
	FT_Face *face = ttra->ft_faces + fontind;
	if (!*face && FT_New_Face(ttra->ft_library, ttra->fontfilenames[fontind], 0, face))
		fprintf(stderr, "epäonnistui %s\n", ttra->fontfilenames[fontind]);

	ttra->fontheight = ttra_set_this_fontheight(ttra, fontind);
	ttra->fontwidth = (*face)->size->metrics.max_advance / 64;
	return ttra->fontheight;
}

void ttra_set_x0(struct ttra *ttra, int x0) {
	ttra_reset(ttra);
	ttra->x0 = ttra->x = x0;
	ttra->w = ttra->realw - x0;
}

void ttra_set_y0(struct ttra *ttra, int y0) {
	ttra_reset(ttra);
	ttra->y0 = y0;
	ttra->h = ttra->realh - y0;
}

void ttra_set_xy0(struct ttra *ttra, int x0, int y0) {
	ttra_reset(ttra);
	ttra->x0 = ttra->x = x0;
	ttra->w = ttra->realw - x0;
	ttra->y0 = y0;
	ttra->h = ttra->realh - y0;
}

static int omit_incomplete_utf8(const char *str, int len) {
	if ((signed char)str[len-1] >= 0)
		return 0;
	for (int back=1; back<=4; back++) {
		switch (str[len-back] >> 6 & 3) {
			case 2:
				continue;
			case 3:
				int thislen = 2;
				for (int i=5; i>=4; i++)
					if (str[len-back] >> i & 1) thislen++;
					else break;
				return back < thislen ? back : 0;
			default:
				return 0;
		}
	}
	fprintf(stderr, "invalid utf-8\n");
	return 0;
}

char* read_pair(char val, int *Print_this) {
	static char paren_invisible[] = "{}";
	static char paren[] = "()[]";

	for (int i=0; i<sizeof(paren_invisible)-2; i+=2)
		if (val == paren_invisible[i]) {
			*Print_this = 0;
			return paren_invisible+i;
		}

	for (int i=0; i<sizeof(paren)-2; i+=2)
		if (val == paren[i]) {
			*Print_this = 1;
			return paren+i;
		}
	*Print_this = 1;
	return "\0 ";
}

void ttra_print(struct ttra *ttra, const char *string) {
	const char *ptr = string;
	char string1[128] = {0};
	int ind = 0;
	char *pair = NULL;
	int pair_nopen = 0, opened_thing = 0, print_this = 0;
	enum {scriptsize_e=1};
	while (1) {
		if (opened_thing) {
			if (*ptr == pair[1]) {
				if (!--pair_nopen) {
					if (opened_thing == scriptsize_e) {
						ttra_rawprint(ttra, string1, ind); ind = 0;
						if (print_this)
							ttra_rawprint(ttra, ptr, 1);
						ttra->style &= ~((__int128)1<<superscript_e | (__int128)1<<subscript_e);
						end_scriptsize(ttra);
						opened_thing = 0;
						++ptr;
					}
				}
			}
			else if (*ptr == pair[0])
				++pair_nopen;
		}

		switch (*ptr) {
			case 0:
				ttra_rawprint(ttra, string1, ind);
				return;
			case 0033:
				ttra_rawprint(ttra, string1, ind); ind = 0;
				ptr = escape(ttra, ptr+1); break;
			case '\n':
				ttra_rawprint(ttra, string1, ind); ind = 0;
				newline(ttra); ptr++; break;
			case '\r':
				ttra_rawprint(ttra, string1, ind); ind = 0;
				carriage_return(ttra); ptr++; break;
			case '\t':
				ttra_rawprint(ttra, string1, ind); ind = 0;
				tabular(ttra); ptr++; break;
			case '\b':
				ttra_rawprint(ttra, string1, ind); ind = 0;
				if (ttra->i > 0) {
					ttra->i--;
					erase_char(ttra);
				}
				if (ttra->max_i < ttra->i)
					ttra->max_i = ttra->i;
				ptr++;
				break;
				/* mathmode */
			case '$':
				if (!ttra->mathmode)
					goto Default;
				ttra_rawprint(ttra, string1, ind); ind = 0;
				ttra->mathmode = 0;
				if (opened_thing == scriptsize_e) {
					ttra->style &= ~((__int128)1<<superscript_e | (__int128)1<<subscript_e);
					end_scriptsize(ttra);
				}
				opened_thing = 0;
				++ptr;
				break;
			case '^':
				if (!ttra->mathmode)
					goto Default;
				ttra_rawprint(ttra, string1, ind); ind = 0;
				ttra->style |= (__int128)1<<superscript_e;
				pair = read_pair(*++ptr, &print_this);
				opened_thing = scriptsize_e;
				if (!print_this) {
					pair_nopen = 1;
					ptr++;
				}
				else if (!pair[0])
					pair_nopen = 1;
				break;
			case '_':
				if (!ttra->mathmode)
					goto Default;
				ttra_rawprint(ttra, string1, ind); ind = 0;
				ttra->style |= (__int128)1<<subscript_e;
				pair = read_pair(*++ptr, &print_this);
				opened_thing = scriptsize_e;
				if (!print_this) {
					pair_nopen = 1;
					ptr++;
				}
				else if (!pair[0])
					pair_nopen = 1;
				break;
			case '-':
				if (!ttra->mathmode)
					goto Default;
				if (sizeof("\u2212")-1 >= sizeof(string1))
					goto Default2;
				memcpy(string1+ind, "\u2212", sizeof("\u2212")-1); // unicode minus
				ind += sizeof("\u2212")-1;
				ptr++;
				goto Default1;
			case '@': // next char literally
				if (ttra->mathmode && ptr[1])
					ptr++;
				goto Default;
				/* end mathmode */
			default:
Default:
				string1[ind++] = *ptr++;
Default1:
				if (ind >= sizeof(string1)) {
					int back = omit_incomplete_utf8(string1, ind);
					ptr -= back;
					ind -= back;
Default2:
					ttra_rawprint(ttra, string1, ind);
					if (ttra->max_i < ttra->i)
						ttra->max_i = ttra->i;
					ind = 0;
				}
		}
	}
	ttra_rawprint(ttra, string1, ind);
	if (ttra->max_i < ttra->i)
		ttra->max_i = ttra->i;
}

void ttra_printf(struct ttra *ttra, const char* form, ...) {
	va_list list;
	va_start(list, form);
	char mem[1024];
	mem[1023] = 0;
	vsnprintf(mem, 1023, form, list);
	va_end(list);
	ttra_print(ttra, mem);
}

void ttra_get_textdims_chars(const char *text, int *width, int *height) {
	struct ttra ttra = {0};
	ttra_print(&ttra, text);
	*width = ttra.max_i;
	*height = ttra.max_j + 1;
}

int ttra_get_nlines(const char *text) {
	struct ttra ttra = {0};
	ttra_print(&ttra, text);
	return ttra.max_j + 1;
}

int ttra_get_width_chars(const char *text) {
	struct ttra ttra = {0};
	ttra_print(&ttra, text);
	return ttra.max_i;
}

int ttra_get_width_pixels(struct ttra *ttra, const char *text) {
	int width, height;
	ttra_get_textdims_pixels(ttra, text, &width, &height);
	return width;
}

int ttra_get_height_pixels(struct ttra *ttra, const char *text) {
	int nlines = ttra_get_nlines(text);
	return nlines * ttra->fontheight;
}

void ttra_get_textdims_pixels(struct ttra *ttra, const char *text, int *width, int *height) {
	save_state(ttra);
	ttra_reset(ttra);
	unsigned *canvas = ttra->canvas;
	ttra->canvas = NULL;
	int wh[] = {ttra->w, ttra->h};
	ttra->w = ttra->h = 1<<30;
	ttra_print(ttra, text);
	ttra->w = wh[0];
	ttra->h = wh[1];
	ttra->canvas = canvas;
	*width = ttra->max_x - ttra->x0;
	*height = (ttra->max_j + 1) * ttra->fontheight;
	restore_state(ttra);
}

void ttra_init(struct ttra *ttra) {
	init_text(ttra);
	if (!ttra->bg_default && !ttra->fg_default) {
		ttra->fg_default = -1;
		ttra->bg_default = 0xff<<24;
	}
	ttra->x = ttra->x0;
	ttra->fontheightsave = 0;
	reset_style(ttra);
	ttra_set_fontheight(ttra, ttra->fontheight ? ttra->fontheight : 16);
}

void __attribute__((destructor)) ttra_fini() {
	FcFini();
}

void ttra_destroy(struct ttra *ttra) {
	destroy_text(ttra);
}

void ttra_reset(struct ttra *ttra) {
	ttra->j = ttra->i = ttra->max_i = ttra->max_j = ttra->max_x = 0;
}
