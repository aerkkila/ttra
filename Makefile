CC = gcc
CFLAGS_static = -Wall -g
CFLAGS_shared = $(CFLAGS_static) -shared -fpic
prefix = /usr/local

libttra.so: ttra.c ttra_rendering.c colors.c ttra.h Makefile
	$(CC) $(CFLAGS_shared) -o $@ $< `pkg-config  --cflags --libs freetype2` -lutf8proc -lfontconfig

ttra.o: ttra.c ttra_rendering.c colors.c fontti.c ttra.h
	$(CC) $(CFLAGS_static) -fpic -c -o $@ $< `pkg-config --cflags freetype2`

wayland_helper/libwayland_helper.so: wayland_helper/*.c wayland_helper/*.h wayland_helper/Makefile
	cd `dirname $@` && $(MAKE)

shared = -lttra -lwaylandhelper -lfontconfig
testi.out: testi.c libttra.so
	gcc -g -Wall -o $@ $< $(shared)

testistatic.out: testi.c ttra.o wayland_helper/wayland_helper.o
	gcc -g -Wall -o $@ $^ `pkg-config --libs freetype2` -lutf8proc -lwayland-client -lxkbcommon

install: libttra.so ttra.h
	cp libttra.so $(prefix)/lib
	cp ttra.h $(prefix)/include

uninstall:
	rm -f $(prefix)/lib/libttra.so $(prefix)/include/ttra.h

clean:
	rm -f *.so *.out
