Terminal style text rendering library
=====================================

See testi.c for usage of this library.
This is used to rasterize text including ansi escape sequences.

The purpose is that this library can be used as a drop-in replacement for graphical programs
to print text to the window rather than to terminal by just changing
printf(...) -> ttra_printf(ttra, ...).
