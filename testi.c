#include <waylandhelper.h>
#include "ttra.h"
#include <unistd.h>

const char *teksti =
"Tavallista tekstiä \033[3mKursiivi\033[1mlihavoitukin\033[31mpunaista\n"
"jatkuu \033[104mkur\033[23msiivitta \033[2mtämän pitäisi\033[0m\033[2m olla heik\033[0mkoa.\n"
"Tämän päälle kirjoitetaan\rtekstiä.\n"
"\033[4Balhaalla\033[A\rylempänä\n99\n"
"joo\bei\n\ba\n"
"aaaaaaabbbbbbbbbbbbbb\033[10Dææ\033[1Kö\n"
"\033[1;2;3;7;91mkäänteinen\033[27mtavallinen\033[m\n"
"\033[1;3;7;91mkäänteinen\033[27mtavallinen\033[m\n"
"ei vielä \033[4mVaan nyt tämä gfd|p \033[7;1malleviivataan.\033[0m\n"
"x⁻²¹ + y = ζ\n"
"abc🔇abc\n"
;

int main() {
	struct waylandhelper wlh = {
		.xresmin = 300,
		.yresmin = 300,
	};
	wlh_init(&wlh);
	wlh_roundtrip(&wlh);
	wlh_commit(&wlh);
	struct ttra ttra = {
		.w = 1000,
		.h = 600,
		.x0 = 10,
		.y0 = 10,
		.fg_color = -1,
		.canvas = wlh.data,
		.fontheight = 24,
		.fonttype = ttra_serif_e,
	};
	ttra_init(&ttra);
	printf("%i\n", ttra.fontheight);
	int maxx, maxy;
	ttra_get_textdims_chars(teksti, &maxx, &maxy);
	printf("rajat x,y = %i,%i\n", maxx, maxy);

	int loppu = 0;
	while (!wlh.stop && wlh_roundtrip(&wlh) >= 0) {
		if (ttra.canvas != wlh.data) {
			ttra_reset(&ttra);
			loppu = 0;
		}
		ttra.canvas = wlh.data;
		ttra.realw = wlh.xres;
		ttra.realh = wlh.yres;
		if (!loppu) {
			ttra_print(&ttra, teksti);
			loppu = 1;
			if (ttra_eol(&ttra)) {
				if (ttra_lastline(&ttra))
					loppu = 1;
				else
					ttra_print(&ttra, "\n");
			}
		}
		wlh_commit(&wlh);
		usleep(50000);
	}
	printf("rajat x,y = %i,%i\n", ttra.max_i, ttra.max_j);
	ttra_destroy(&ttra);
	wlh_destroy(&wlh);
}
