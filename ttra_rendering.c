#include <utf8proc.h>
#include <err.h>
#include <assert.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#define __included_freetype2__
#include <fontconfig/fontconfig.h>
#define __included_fontconfig__
#include "ttra.h"

#include "fontti.c"

static inline int my_iround(float a) {
	int i = a;
	return a >= 0 ? i + (a-i >= 0.5) : i - (i-a >= 0.5);
}

static inline int my_iroundpos(float a) {
	int i = a;
	return i + (a-i >= 0.5);
}

static inline int my_iceil(float a) {
	int i = a;
	return i + (a-i >= 1e-10);
}

static int some_kind_of_hash1000(const char *str, unsigned ind) {
	int len = strlen(str);
	int num = 0;
	for (int i=0; i<len; i++)
		num ^= ((str[i]^i)+i) << i%3;
	char sind[sizeof(ind)];
	memcpy(sind, &ind, sizeof(ind));
	for (int i=0; i<sizeof(ind); i++)
		num ^= ((sind[i]^i)+i) << i%3;
	return num % 1000;
}

static void read_fontinfo(struct ttra *ttra, struct __fontinfo *fontinfo) {
	int nfonts = 0;
	for (int istyle=0; istyle<nfontstyles; istyle++)
		nfonts += ttra->nfont[istyle] = fontinfo[istyle].nfont;
	int ndifferent = 0;
	int *indices = malloc(nfonts * sizeof(int));
	int *indices_ptr = indices;

	unsigned short used[1000+nfonts+1];
	memset(used, 0, sizeof(used));
	for (int istyle=0; istyle<nfontstyles; istyle++) {
		ttra->font_indices[istyle] = indices_ptr;
		indices_ptr += ttra->nfont[istyle];
		for (int i=0; i<ttra->nfont[istyle]; i++) {
			int hash = some_kind_of_hash1000(fontinfo[istyle].fontfiles[i], fontinfo[istyle].faceindices[i]);
			while (used[hash]) {
				int jstyle = used[hash] & 0xff;
				int j = used[hash] >> 8 & 0xff;
				if (!strcmp(fontinfo[istyle].fontfiles[i], fontinfo[jstyle].fontfiles[j]) &&
					fontinfo[istyle].faceindices[i] == fontinfo[jstyle].faceindices[j])
				{
					ttra->font_indices[istyle][i] = ttra->font_indices[jstyle][j];
					goto continue_for2;
				}
				hash++;
			}
			used[hash] = i<<8 | istyle;
			ttra->font_indices[istyle][i] = ndifferent++;
continue_for2:;
			  asm ("nop");
		}
	}

	ttra->ndifferent_fonts = ndifferent;
	ttra->ft_faces = calloc(ndifferent, sizeof(FT_Face));
	ttra->charsets = calloc(ndifferent, sizeof(void*));
	ttra->fontfilenames = malloc(ndifferent * sizeof(void*));
	ttra->current_fontheight = calloc(ndifferent, sizeof(short));
	ttra->faceindices = calloc(ndifferent, sizeof(short));

	for (int istyle=0; istyle<nfontstyles; istyle++)
		for (int i=0; i<ttra->nfont[istyle]; i++) {
			int ind = ttra->font_indices[istyle][i];
			if (ttra->charsets[ind]) {
				free(fontinfo[istyle].fontfiles[i]);
				continue;
			}
			ttra->charsets[ind] = fontinfo[istyle].charsets[i];
			ttra->fontfilenames[ind] = fontinfo[istyle].fontfiles[i];
			ttra->faceindices[ind] = fontinfo[istyle].faceindices[i];
		}
}

static void init_text(struct ttra *ttra) {
	assert(!FT_Init_FreeType(&ttra->ft_library));

	static const char *stylestr_arr[][4] = {
		[ttra_mono_e] = {"mono", "mono:bold", "mono:italic", "mono:bold:italic"},
		[ttra_serif_e] = {"serif", "serif:bold", "serif:italic", "serif:bold:italic"},
		[ttra_sans_e] = {"sans", "sans:bold", "sans:italic", "sans:bold:italic"},
	};
	const char **stylestr = stylestr_arr[ttra->fonttype];
	struct __fontinfo fontinfo[nfontstyles];
	for (int istyle=0; istyle<nfontstyles; istyle++)
		fontinfo[istyle] = get_fonts(stylestr[istyle]);
	read_fontinfo(ttra, fontinfo);

	for (int istyle=0; istyle<nfontstyles; istyle++) {
		free(fontinfo[istyle].faceindices);
		free(fontinfo[istyle].fontfiles);
		free(fontinfo[istyle].charsets);
	}
	ttra->text_initialized = 1;
}

static void destroy_text(struct ttra *ttra) {
	FT_Done_FreeType(ttra->ft_library);
	free(ttra->ft_faces);
	ttra->ft_library = (FT_Library){0};
	for (int i=0; i<ttra->ndifferent_fonts; i++)
		free(ttra->fontfilenames[i]);
	free(ttra->charsets);
	free(ttra->fontfilenames);
	free(ttra->faceindices);
	free(ttra->current_fontheight);
	free(ttra->font_indices[0]);
	ttra->text_initialized = 0;
}

static void end_scriptsize(struct ttra *ttra) {
	if (ttra->fontheightsave)
		ttra_set_fontheight(ttra, ttra->fontheightsave);
	ttra->fontheightsave = 0;
}

static void begin_scriptsize(struct ttra *ttra) {
	ttra->fontheightsave = ttra->fontheight;
	ttra_set_fontheight(ttra, ttra->fontheight*0.6);
}

static void apply_faint(struct ttra *ttra) {
	int new_fg = 0xff<<24;
	for (int i=0; i<3; i++) {
		int ca = ttra->fg_color >> i*8 & 0xff;
		int cb = ttra->bg_color >> i*8 & 0xff;
		ca += (cb - ca) * 0.4; // how much towards background color
		new_fg |= ca << i*8;
	}
	ttra->fg_color = new_fg;
}

static void underline_between(struct ttra *ttra, int x0, int x1, int baseline, float ypixels_per_unit) {
	int ind = ttra->font_indices[ttra->fontstyle][0];
	float h = ttra->ft_faces[ind]->underline_thickness * ypixels_per_unit;
	float y0f = baseline - ttra->ft_faces[ind]->underline_position * ypixels_per_unit - h/2;
	int ih = my_iroundpos(h);
	if (ih < 1) ih = 1;
	int y1 = my_iroundpos(y0f + h);
	int y0 = y1 - ih;
	/*int x1 = ttra->x0 + ttra->i * ttra->fontwidth;
	  int x0 = x1 - len * ttra->fontwidth;*/

	if (x0 < ttra->x0) x0 = ttra->x0;
	if (x1 > ttra->x0 + ttra->w) x1 = ttra->x0 + ttra->w;
	if (y0 < ttra->y0) y0 = ttra->y0;
	if (y1 > ttra->y0 + ttra->h) y1 = ttra->y0 + ttra->h;

	unsigned color = ttra->fg_color;
	for (int j=y0; j<y1; j++) {
		unsigned *ptr = ttra->canvas + j * ttra->realw;
		for (int i=x0; i<x1; i++)
			ptr[i] = color;
	}
}

static inline void ttra_check_fontsize(struct ttra *ttra, int ind) {
	if (ttra->current_fontheight[ind] != ttra->fontheight)
		ttra_set_this_fontheight(ttra, ind);
}

static FT_Face get_font(struct ttra *ttra, uint32_t character) {
	int ifont, ind=-1;
	for (ifont=0; ifont<ttra->nfont[ttra->fontstyle]; ifont++) {
		ind = ttra->font_indices[ttra->fontstyle][ifont];
		if (FcCharSetHasChar(ttra->charsets[ind], character))
			goto found;
	}
	ifont = 0;
found:
	FT_Face *face = ttra->ft_faces + ind;
	if (!*face && FT_New_Face(ttra->ft_library, ttra->fontfilenames[ind], ttra->faceindices[ind], face))
		fprintf(stderr, "epäonnistui %s\n", ttra->fontfilenames[ind]);
	ttra_check_fontsize(ttra, ind);
	return *face;
}

unsigned char* ttra_get_bitmap(struct ttra *ttra, const char* text, int *width, int *height) {
	int32_t characters[1], len;
	if ((len = utf8proc_decompose((const utf8proc_uint8_t*)text, strlen(text), characters, 1, 0)) < 0)
		warn("utf8proc_decompose");
	FT_Face ftface = get_font(ttra, characters[0]);
	ftface->glyph->format = FT_GLYPH_FORMAT_BITMAP;
	int charind = FT_Get_Char_Index(ftface, characters[0]);
	assert(!FT_Load_Glyph(ftface, charind, 0));
	assert(!FT_Render_Glyph(ftface->glyph, 0)); // Should this only be initialized once?
	FT_GlyphSlot glyph = ftface->glyph;
	FT_Bitmap *map = &glyph->bitmap;
	*width = map->width;
	*height = map->rows;
	return map->buffer;
}

void ttra_rawprint(struct ttra *ttra, const char* text, int len) {
	if (!len)
		return;

	int32_t characters[len];
	if ((len = utf8proc_decompose((const utf8proc_uint8_t*)text, len, characters, len, 0)) < 0)
		warn("utf8proc_decompose");

	if (!ttra->text_initialized) {
		ttra->i += len;
		goto end;
	}

	if (ttra->i == 0)
		ttra->x = ttra->x0;

	end_scriptsize(ttra);
	FT_Face ftface = get_font(ttra, characters[0]);
	float ypixels_per_unit = (float)ftface->size->metrics.y_ppem / ftface->units_per_EM;
	//int slot_left = ttra->x0 + ttra->i * ttra->fontwidth;
	int slot_left = ttra->x;
	int slot_top = ttra->y0 + ttra->j * ttra->fontheight;
	int slot_bottom = slot_top + ttra->fontheight;
	int baseline = my_iround(slot_bottom + ftface->bbox.yMin * ypixels_per_unit);
	slot_bottom = min(slot_bottom, ttra->y0 + ttra->h);
	if (slot_left >= ttra->x0 + ttra->w || slot_top >= ttra->y0 + ttra->h)
		return;

	unsigned *canvas = ttra->canvas;

	int fg_old = ttra->fg_color;
	int bg_old = ttra->bg_color;
	if (ttra->style & 1<<invert_e) {
		ttra->fg_color = bg_old;
		ttra->bg_color = fg_old;
	}
	if (ttra->style & 1<<faint_e)
		apply_faint(ttra);
	if (ttra->style & (__int128)1<<subscript_e) {
		int bottom = slot_top + ttra->fontheight;
		begin_scriptsize(ttra);
		/* ftface is a pointer and now different than before */
		float ypixels_per_unit1 = (float)ftface->size->metrics.y_ppem / ftface->units_per_EM;
		int bottomnow = my_iround(baseline - ftface->bbox.yMin * ypixels_per_unit1);
		baseline += (bottom - bottomnow) / 2;
	}
	if (ttra->style & (__int128)1<<superscript_e) {
		begin_scriptsize(ttra);
		int bottom = slot_top + ttra->fontheight;
		/* ftface is a pointer and now different than before */
		float ypixels_per_unit = (float)ftface->size->metrics.y_ppem / ftface->units_per_EM;
		baseline = my_iround(bottom + ftface->bbox.yMin * ypixels_per_unit);
	}

	int xstart = ttra->x;

	int right = ttra->x0 + ttra->w;
	for (int c=0; c<len; c++) {
		if (c) ftface = get_font(ttra, characters[c]);
		ftface->glyph->format = FT_GLYPH_FORMAT_BITMAP;
		int charind = FT_Get_Char_Index(ftface, characters[c]);
		assert(!FT_Load_Glyph(ftface, charind, 0));
		int slotwidth = my_iceil(ftface->glyph->advance.x / 64);
come_from_newline:
		slot_left = ttra->x;
		int slot_right = slot_left + slotwidth;
		if (slot_right > right) {
			if (ttra->chop_lines || ttra->i == 0) {
				if (slot_left > right)
					break;
				else
					slot_right = right;
			}
			else {
				if (ttra->max_i < ttra->i)
					ttra->max_i = ttra->i;
				if (ttra->max_x < ttra->x)
					ttra->max_x = ttra->x;
				newline(ttra);
				slot_top = ttra->y0 + ttra->j * ttra->fontheight;
				slot_bottom = slot_top + ttra->fontheight;
				baseline = my_iround(slot_bottom + ftface->bbox.yMin * ypixels_per_unit);
				slot_bottom = min(slot_bottom, ttra->y0 + ttra->h);
				ttra->x = ttra->x0;
				goto come_from_newline;
			}
		}
		ttra->x += slotwidth;
		ttra->i++;
		if (!canvas)
			continue;

		assert(!FT_Render_Glyph(ftface->glyph, 0));
		FT_GlyphSlot glyph = ftface->glyph;
		FT_Bitmap *bitm = &glyph->bitmap;
		int bw = bitm->width,
			bh = bitm->rows;

		int letter_top = min(baseline - glyph->bitmap_top, ttra->y0 + ttra->h);
		if (letter_top < 0)
			letter_top = 0;
		int letter_bottom = min(letter_top + bh, ttra->y0 + ttra->h);
		int letter_left = slot_left + glyph->bitmap_left;
		int left_crop = ttra->x0 - letter_left > 0 ? ttra->x0 - letter_left : 0; // if bitmap_left < 0
		int letter_right = min(letter_left + bw, ttra->x0 + ttra->w);

		/* Fill the empty space around the letter */
		int dataind_y = slot_left + slot_top * ttra->realw;
		int dataind = dataind_y;
		if (ttra->bg_color >> 24 == 0)
			goto no_background;
		for (int j=slot_top; j<letter_top; j++) {
			for (int i=slot_left; i<slot_right; i++)
				canvas[dataind++] = ttra->bg_color;
			dataind = dataind_y += ttra->realw;
		}
		dataind_y = slot_left + letter_top * ttra->realw;
		for (int j=letter_top; j<letter_bottom; j++) {
			for (int i=slot_left; i<letter_left; i++)
				canvas[dataind++] = ttra->bg_color;
			dataind = dataind_y - slot_left + letter_right;
			for (int i=letter_right; i<slot_right; i++)
				canvas[dataind++] = ttra->bg_color;
			dataind = dataind_y += ttra->realw;
		}
		dataind = dataind_y = slot_left + letter_bottom * ttra->realw;
		for (int j=letter_bottom; j<slot_bottom; j++) {
			for (int i=slot_left; i<slot_right; i++)
				canvas[dataind++] = ttra->bg_color;
			dataind = dataind_y += ttra->realw;
		}

		dataind = dataind_y = letter_left + left_crop + letter_top * ttra->realw;
		int width = letter_right - letter_left;
		int height = letter_bottom - letter_top;

		for (int j=0; j<height; j++) {
			for (int i=left_crop; i<width; i++) {
				unsigned value = ((unsigned char*)bitm->buffer)[bw*j + i];
				unsigned eulav = 255-value;
				//canvas[dataind++] = 0xff<<24 | value<<16 | value<<8 | value<<0;
				int r = ttra->bg_color >> 16 & 0xff,
					g = ttra->bg_color >> 8 & 0xff,
					b = ttra->bg_color >> 0 & 0xff,
					R = ttra->fg_color >> 16 & 0xff,
					G = ttra->fg_color >> 8 & 0xff,
					B = ttra->fg_color >> 0 & 0xff;
				r = (r * eulav + R * value) / 255;
				g = (g * eulav + G * value) / 255;
				b = (b * eulav + B * value) / 255;
				canvas[dataind++] = 0xff<<24 | r << 16 | g << 8 | b << 0;
			}
			dataind = dataind_y += ttra->realw;
		}

		if (0) {
no_background:
			dataind = dataind_y = letter_left + left_crop + letter_top * ttra->realw;
			int width = letter_right - letter_left;
			int height = letter_bottom - letter_top;

			int fg = ttra->fg_color;
			for (int j=0; j<height; j++) {
				for (int i=left_crop; i<width; i++) {
					unsigned value = ((unsigned char*)bitm->buffer)[bw*j + i];
					unsigned eulav = 255-value;
					int bg = canvas[dataind];
					//canvas[dataind++] = 0xff<<24 | value<<16 | value<<8 | value<<0;
					int r = bg >> 16 & 0xff,
						g = bg >> 8 & 0xff,
						b = bg >> 0 & 0xff,
						R = fg >> 16 & 0xff,
						G = fg >> 8 & 0xff,
						B = fg >> 0 & 0xff;
					r = (r * eulav + R * value) / 255;
					g = (g * eulav + G * value) / 255;
					b = (b * eulav + B * value) / 255;
					canvas[dataind++] = 0xff<<24 | r << 16 | g << 8 | b << 0;
				}
				dataind = dataind_y += ttra->realw;
			}
		}
	}

	if (!canvas)
		goto end;

	if (ttra->style & 1<<underlined_e)
		underline_between(ttra, xstart, ttra->x, baseline, ypixels_per_unit);

	if (ttra->clean_line)
		for (int j=0; j<ttra->fontheight; j++)
			for (int i=ttra->x; i<ttra->w; i++)
				canvas[(j+slot_top)*ttra->realw + i] = ttra->bg_color;

	ttra->fg_color = fg_old;
	ttra->bg_color = bg_old;

end:
	if (ttra->max_i < ttra->i)
		ttra->max_i = ttra->i;
	if (ttra->max_x < ttra->x)
		ttra->max_x = ttra->x;
}
