#ifndef __ttra_h__
#define __ttra_h__

enum ttra_style {
	normal_e, bold_e=1, faint_e=2, italic_e=3, underlined_e=4, invert_e=7,
	not_bold_e=22, not_italic_e=23, not_underlined_e=24, not_invert_e=27,
	superscript_e=73, subscript_e=74, not_script_e=75,
};
enum ttra_fontstyle {regularfont_e, boldfont_e, italicfont_e, bolditalicfont_e};
enum ttra_fonttype {ttra_mono_e, ttra_serif_e, ttra_sans_e};
#define nfontstyles 4

#ifdef __included_fontconfig__
#define __FCtype(type) type
#else
#define __FCtype(type) void*
#endif

#ifdef __included_freetype2__
#define __FTtype(type) type
#else
#define __FTtype(type) void*
#endif

struct ttra {
	int i, j, x, w, h, x0, y0, realw, realh, isave, jsave, xsave, fontwidth, fontheight, fontheightsave, text_initialized;
	int max_i, max_j, max_x;
	unsigned char chop_lines:1, mathmode:1;
	unsigned fg_color, bg_color, *canvas, fg_default, bg_default;
	unsigned __attribute__((mode(TI))) style; // uint128
	enum ttra_fontstyle fontstyle;
	enum ttra_fonttype fonttype;
	int clean_line, nfont[nfontstyles], ndifferent_fonts;
	int *font_indices[nfontstyles];
	__FTtype(FT_Library) ft_library;
	__FTtype(FT_Face) *ft_faces;
	__FCtype(FcCharSet*) *charsets;
	char **fontfilenames;
	short *current_fontheight, *faceindices;
};

#undef __FCtype
#undef __FTtype

unsigned char* ttra_get_bitmap(struct ttra *ttra, const char* text, int *width, int *height);
void ttra_get_textdims_chars(const char *text, int *width, int *height);
int  ttra_get_nlines(const char *text);
int  ttra_get_width_chars(const char *text);
void ttra_get_textdims_pixels(struct ttra *ttra, const char *text, int *width, int *height);
int  ttra_get_width_pixels(struct ttra *ttra, const char *text);
int  ttra_get_height_pixels(struct ttra *ttra, const char *text);
int  ttra_set_fontheight(struct ttra *ttra, int height);
int  ttra_set_this_fontheight(struct ttra *ttra, int fontind);
void ttra_set_x0(struct ttra *ttra, int x0);
void ttra_set_y0(struct ttra *ttra, int y0);
void ttra_set_xy0(struct ttra *ttra, int x0, int y0);
int  ttra_end(struct ttra *tr);
int  ttra_lastline(struct ttra *tr);
int  ttra_eol(struct ttra *tr);
void ttra_init(struct ttra *tr);
void ttra_fini();
void ttra_destroy(struct ttra *tr);
void ttra_reset(struct ttra *tr);
void ttra_rawprint(struct ttra *tr, const char* text, int length); // Doesn't handle escape sequences.
void ttra_print(struct ttra *ttra, const char *string); // Handles escape sequences.
void ttra_printf(struct ttra *ttra, const char* form, ...);

#endif
